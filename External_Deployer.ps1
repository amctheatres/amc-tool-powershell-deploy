﻿###########  USER CHANGE VARIABLES  #############

$exclusions = @('server.json', 'client.json', 'appsettings.Production', 'config.json')

$GraphPackageDirectory = 'GraphQL (Prod - On Commit)'
$ContentAPIPackageDirectory = 'Content Api (Prod - On Commit)'
$ContentAdminPackageDirectory = 'Content Admin (Prod - On Commit)'
$WebPackageDirectory = 'Website (Prod)'

$ExpectedHTTP = 200

$ContentAPIExtraDirectory = 'AmcContentAPI-v1'
$ContentAdminExtraDirectory = 'Amc.Admin.Web'

$PrimaryServer = hostname

$StageTestMode = "false"

#################################################

###########  VALIDATION TESTING  ################
function test() 
{
    param(
        $application,
        $environment,
        $directory,
        $distro
    )
    $iter = 0 

    do {
        $latest = Get-ChildItem -Path "C:\$directory\$application" | where { ! $_.PSIsContainer } | Sort-Object LastAccessTime -Descending | Select-Object -First 1 -Skip $iter
        
        if ($latest -eq $null) {
            break
        }
        
        $LatestLocalFile = $latest.name

        #echo $LatestLocalFile

        if ($LatestLocalFile -eq "config.json") {
            break
        }

        $diff = Compare-Object -ReferenceObject $(get-content "C:\$directory\$application\$LatestLocalFile") -DifferenceObject $(get-content "C:\Temp\Content\C_C\J\workspace\$distro\dist\$LatestLocalFile")

        if ($diff -ne $null) {
            echo "A difference was detected between distribution and local machine on the file $LatestLocalFile"
            pause
        }

        $iter++
    } while ($iter -lt 5)

    #$count = (Get-ChildItem -Path "C:\$directory\$application" | Measure-Object).count

    if ($application -eq 'graph') {
        $HTTP_Request = [system.net.webrequest]::create('http://localhost/query')
        $ExpectedHTTP = 200
    }

    if ($application -eq 'website') {
        $HTTP_Request = [system.net.webrequest]::create('http://localhost')
        $ExpectedHTTP = 200
    }
    
    $HTTP_Response = $HTTP_Request.getresponse()

    $HTTP_Status = [int]$HTTP_Response.statuscode

    echo "The HTTP response is $HTTP_Status"

    if ($HTTP_Status -ne $ExpectedHTTP) {
        echo "THE VERIFICATION TEST HAS FAILED"
        exit
    }

    $HTTP_Response.close()

}
#hit a website using powershell and check for a 200 response?


#################################################

#test -application "graph" -environment "test" -directory "Services" -distro "GraphQL (Prod - On Commit)"

$username = $env:username

$uname = "homeoffice\$username"

$primaryhost = hostname

#Unzip function because of Powershell 4.0 usage
function unzip {
    echo "Unzipping files"
    add-type -assembly "system.io.compression.filesystem"
    [io.compression.zipfile]::extracttodirectory("C:\temp\$packagename.zip", "C:\temp")
}

#Exclusions 

#Clearing contents of Temp directory from previous runs
rm C:\Temp\* -recurse -force

start-sleep 5

#Prompts to populate package name, application, environment, whether the package is located in Azure or on the desktop and CyberArk credentials
#environment and application are then used to determine expected directory structure of package, number of servers, which is the primary server and, if prod, which nodes need to be set offline
$packagename = read-host -prompt 'Please provide the name of the package to download: '

$primarylength = $primaryhost.length

$environmenttype = $primaryhost.substring($primarylength-3,1)

if ($environmenttype -eq "T") {
    $environment = "test"
}

if ($environmenttype -eq "P") {
    $environment = "prod"
}

if ($environmenttype -eq "S") {
    $environment = "stage"
}

if ($environmenttype -eq "D") {
    $environment = "dev"
}

$applicationtype = $primaryhost.substring($primarylength-6,3)

if ($applicationtype -eq "GPH") {
    $application = 'graph'
}

if ($applicationtype -eq "WEB") {
    $application = 'website'
    $applicationtype = 'EB'
}

if ($applicationtype -eq "CON") {
    $application = 'content api'
}

$f = 1

do { 
    
    if ($f -lt 10) {
        $server = "SVD0DMZW" + $applicationtype  + $environmenttype + "0" + $f.ToString()
    }

    if ($f -ge 10) {
        $server = "SVD0DMZW" + $applicationtype + $environmenttype + $f.ToString()
    }
    
    $pingerror = Test-Connection -computer $server -Quiet
    $f++
} while ($pingerror -eq "True")

$servernumber = $f - 1
$foundservers = $servernumber - 1
echo "$foundservers servers found"

#$application = read-host -prompt 'What application? (Graph, Website, Content API)'

    if (($Application -ne 'Graph') -and ($Application -ne 'website') -and ($Application -ne 'content api')) {
        echo "Please choose a valid application"
        exit
    }

#$environment = read-host -prompt 'Prod or Stage?'

    if (($environment -ne 'prod') -and ($environment -ne 'stage') -and ($environment -ne 'test') -and ($environment -ne 'dev')) {
        echo "Please choose either prod, stage, dev or test"
        exit
    }

    <#if (($environment -eq 'stage') -and ($application -eq 'graph')) {
        $server = 'SVD0DMZWGPHS01'
        #$servernumber = 3
		$distro = 'GraphQL (Prod - On Commit)'

    }#>

    if ($application -eq "graph") {
        $distro = $GraphPackageDirectory
    }

    if ($application -eq "website") {
        $distro = $WebPackageDirectory
    }
    
    if ($application -eq 'content api') {
		$distro = $ContentAPIPackageDirectory
    }

    if (($environment -eq 'prod') -and ($application -eq 'content api')) {
        echo "Please stop 192.168.250.181 and 192.168.250.182"
        pause
    }

    if (($environment -eq 'stage') -and ($application -eq 'content api') -and ($StageTestMode -eq 'true')) {
        echo "Please stop 192.168.250.181 and 192.168.250.182"
        pause
    }

    if (($environment -eq 'prod') -and ($application -eq 'graph')) {
        #$server = 'SVD0DMZWGPHP01'
        #$servernumber = 23
		#$distro = 'GraphQL (Prod - On Commit)'
		echo "Please stop 192.168.250.191 - 197"
		pause
    }

    if (($environment -eq 'stage') -and ($application -eq 'graph') -and ($StageTestMode -eq 'true')) {
        #$server = 'SVD0DMZWGPHP01'
        #$servernumber = 23
		#$distro = 'GraphQL (Prod - On Commit)'
		echo "Please stop 192.168.250.171 - 174"
		pause
    }

    <#if (($environment -eq 'stage') -and ($application -eq 'website')) {
        $server = 'SVD0DMZWEBS01'
        #$servernumber = 7
		$distro = 'Website (Prod)'
    }#>

    if (($environment -eq 'prod') -and ($application -eq 'website')) {
        #$server = 'SVD0DMZWEBP01'
        #$servernumber = 11
		#$distro = 'Website (Prod)'
		echo "Please stop 192.168.250.171 - 174"
		pause
    }

    if (($environment -eq 'stage') -and ($application -eq 'website') -and ($StageTestMode -eq 'true')) {
        #$server = 'SVD0DMZWEBS01'
        #$servernumber = 11
		#$distro = 'Website (Prod)'
		echo "Please stop 192.168.250.171 - 174"
		pause
    }

    <#if (($environment -eq 'stage') -and ($application -eq 'content api')) {
        $server = 'SVD0DMZWCONS01'
        #$servernumber = 3
		$distro = 'Content Api (Stage - On Commit)'
    }

    if (($environment -eq 'prod') -and ($application -eq 'content api')) {
        $server = 'SVD0DMZWCONP01'
        #$servernumber = 5
		$distro = 'Content API (Prod - On Commit)'
    }#>

    if ($application -eq 'website') {
        $directory = 'Sites'
    }

    if ($application -eq 'graph') {
        $directory = 'Services'
    }

    if ($application -eq 'content api') {
        $directory = 'Services'
    }

    if (($application -ne 'website') -and ($application -ne 'graph') -and ($application -ne 'content api')) {
        echo "Directory copy structure not found"
        exit
    }

$secondaryhosts = ''
$i = 2

#$DownOrDesk = read-host -prompt 'Download from Azure or unzip from desktop? (a,u)'
$DownOrDesk = 'u'

if (($DownOrDesk -ne 'a') -and ($DownOrDesk -ne 'u')) {
	echo "Please choose a valid input"
	exit
}

#Check to make sure package exists on Desktop before moving on
$ZipPackagetester = 0

if ((test-path -path C:\Users\$username\desktop\$packagename.zip -pathtype leaf) -and ($DownOrDesk -eq 'u')) {
	$ZipPackagetester = 1
}

if ($DownOrDesk -eq 'a') {
	$ZipPackagetester = 1
}

if ($ZipPackagetester -eq 0) { 
	echo "Package not found"
	exit
}

if ($application -ne 'content api') {
    if (($application -ne 'graph') -and ($environment -ne 'prod')) {
## Initial testing to make sure sites are up BEFORE deployment
    if ($application -eq 'graph') {
        $HTTP_Request = [system.net.webrequest]::create('http://localhost/query')
        $ExpectedHTTP = 200
    }

    if ($application -eq 'website') {
        $HTTP_Request = [system.net.webrequest]::create('http://localhost')
        $ExpectedHTTP = 200
    }
    
    $HTTP_Response = $HTTP_Request.getresponse()

    $HTTP_Status = [int]$HTTP_Response.statuscode

    echo "The HTTP response is $HTTP_Status for the primary server."

    if ($HTTP_Status -ne $ExpectedHTTP) {
        echo "THE PRE-DEPLOYMENT VERIFICATION TEST HAS FAILED"
        $HTTP_Response.close()
        exit
    }
    else {
        echo "Pre-Deployment Verification Test was Successful"
    }

    $HTTP_Response.close()
    }
}
read-host -prompt 'CyberArk Password? ' -assecurestring | ConvertFrom-SecureString | out-file C:\Users\$username\documents\securestring.txt

$password = get-content "C:\Users\$username\documents\securestring.txt" | ConvertTo-SecureString

#Azure download
if ($DownOrDesk -eq 'a') {

	$folder = "c:\temp"
	$url = "https://cheffileshare.blob.core.windows.net/packages/$packagename.zip"
	$req = [System.Net.HttpWebRequest]::create($url)
	$req.method = "HEAD"
	$response = $req.GetResponse()
	$fUri = $response.ResponseUri
	$filename = [system.io.path]::getfilename($furi.LocalPath);
	$response.Close()
	$target = join-path $folder $filename
	Invoke-WebRequest -uri $url -outfile $target

}

if ($DownOrDesk -eq 'u') {
    echo "Copying zip package"
	copy-item -path C:\users\$username\desktop\$packagename.zip -destination C:\temp\$packagename.zip
}

unzip

    echo "Backing up files"
    #Usage of preexisting backup batch file
    C:\BackupWebsites.bat | Out-Null
    echo "Stopping Services on primary server"
    stop-service W3SVC
    
    #Creation of credentials file for use when invoking commands on other servers            
    $cred = New-Object -TypeName system.management.automation.pscredential -argumentlist $uname, $password
	
	echo "Copying unzipped files to the primary server"

    $BackupDirectory = Get-Date -UFormat "%m-%d-%Y"

    #Files are copied over from the unzipped package in the Temp directory to the primary server application directory with exclusions for config files
    if ($application -eq 'graph') {
        #copy "C:\Temp\Content\C_C\J\workspace\$distro\dist\*" -destination "C:\Services\$application\" -recurse -force
        get-childitem "C:\Temp\Content\C_C\J\workspace\$distro\dist" -Exclude $exclusions | Copy-Item -destination "C:\Services\$application\" -recurse -force -exclude client.json, server.json
    }
	
	if ($application -eq 'website') {
		#copy "C:\Temp\Content\C_C\J\workspace\$distro\dist\*" -destination "C:\Sites\$application\" -recurse -force

        #Get-ChildItem $from -Directory | Where-Object{$_.Name -notin $excludes} | Copy-Item -Destination $to -recurse -force           

        get-childitem "C:\Temp\Content\C_C\J\workspace\$distro\dist" -Exclude $exclusions | Copy-Item -destination "C:\Sites\$application\" -recurse -force -exclude client.json, server.json
        Copy-Item C:\Backup\$BackupDirectory\Sites\website\config\server.json -Destination C:\Sites\website\config\server.json -force
        Copy-Item C:\Backup\$BackupDirectory\Sites\website\config\client.json -Destination C:\Sites\website\config\client.json -force
        #Cleaning up files created by get-childitem recursive copying

	}

    if (($application -eq 'content api') -or ($application -eq 'content admin')) {
        
        if ($application -eq 'content api') {
            $extra = $ContentAPIExtraDirectory
            $directory = 'Services'
            $application = 'content api'
            $copydirectory = 'content'
        }

        if ($application -eq 'content admin') {
            $extra = $ContentAdminExtraDirectory
            $application = 'admin'
            $directory = 'Sites'
        }

		#copy "C:\Temp\Content\C_C\J\workspace\$distro\src\$extra\bin\Release\netcoreapp1.1\publish" -destination "C:\$dir\$application\" -recurse -force -exclude server.json, client.json, appsettings.Production, config.json
		get-childitem "C:\Temp\Content\C_C\J\workspace\$distro\src\$extra\bin\Release\netcoreapp1.1\publish" -Exclude $exclusions | Copy-Item -destination "C:\$directory\$copydirectory\" -recurse -force -exclude client.json, server.json
        if (($environment -eq 'prod') -and ($application -eq 'content api')) {
            Copy-Item C:\Backup\$BackupDirectory\Services\content\appsettings.json -Destination C:\Services\content\appsettings.json -force
            Copy-Item C:\Backup\$BackupDirectory\Services\content\appsettings.Production.json -Destination C:\Services\content\appsettings.Production.json -force
        }
        if (($environment -eq 'stage') -and ($application -eq 'content api')) {
            Copy-Item C:\Backup\$BackupDirectory\Services\content\appsettings.json -Destination C:\Services\content\appsettings.json -force
        }
	}

    echo "Starting services on primary server"
	start-service W3SVC
	$hostedname = hostname
	echo "$hostedname has been completed"
	
    #Creating secondary hostname template using primary hostname
	$PrimaryHostLength = $PrimaryHost.length - 2
    $PrimaryHost = $PrimaryHost.substring(0,$PrimaryHostLength)

    $SecondaryHosts = $PrimaryHost

    #Stop servers named, these servers will be the halfway point for all active F5 nodes
    if (($environment -eq "prod") -and ($application -eq "graph")) {
        $stopservernumber = $servernumber/2
        $stopservernumber = [math]::floor($stopservernumber)
        <#
        if ($stopservernumber -lt 10) {
            $stopservername = "SVD0DMZW" + $applicationtype + "0" + $stopservernumber
        }

        if ($stopservernumber -ge 10) {
            $stopservername = "SVD0DMZW" + $applicationtype + $stopservernumber
        }#>

		$stopservername = "SVD0DMZWGPHP08.perimeter.amc.corp"
    }

    if (($environment -eq "prod") -and ($application -eq "content api")) {
		$stopservername = "SVD0DMZWCONP03.perimeter.amc.corp"
    }

    if (($environment -eq "prod") -and ($application -eq "website")) {
		$stopservername = "SVD0DMZWEBP05.perimeter.amc.corp"
    }

    if (($environment -eq "stage") -and ($application -eq "website") -and ($StageTestMode -eq "true")) {
		$stopservername = "SVD0DMZWEBS04.perimeter.amc.corp"
    }

    #Loop to copy files from primary host to all secondary hosts.  It will also check to see if it has arrived at the mid point F5 node and will then stop to wait for the first half
    #of the nodes to be activated and the second half to be deactivated
    do {
	   #This if is for all servers that are below 10 since the naming will be different.  This happens because $i is an integer and will need to be injected into a string 	
       if ($i -lt 10) {

	    	$SecondaryHostName = $SecondaryHosts + "0" + $i + ".perimeter.amc.corp"
            $hostedname = $secondaryhosts + "0" + $i
            
            #Check to see if the stop server has been reached
			if (($stopservername -eq $secondaryhostname) -and ($environment -eq "prod")) {
				write-host 'Press any key once the first half of the F5 nodes have been enabled and the last half have been disabled'
				if ($application -eq "website") {
                    echo "Please start 192.168.250.171 - 192.168.250.174"
					echo "Please stop 192.168.250.175 - 178"
				}
				
				if ($application -eq "graph") {
                    echo "Plesae start 192.168.250.191 - 192.168.250.197"
					echo "Please stop 192.168.250.198 - 204"
				}
				
				if ($application -eq "content api") {
                    echo "Please start 192.168.250.181 and 192.168.250.182"
					echo "Please stop 192.168.250.183"
				}
				
                pause
			}

			if (($stopservername -eq $secondaryhostname) -and ($environment -eq "stage") -and ($StageTestMode -eq "true")) {
				write-host 'Press any key once the first half of the F5 nodes have been enabled and the last half have been disabled'
				if ($application -eq "website") {
					echo "Please start first half of the servers and stop second half"
				}
				
				if ($application -eq "graph") {
					echo "Please start first half of the servers and stop second half"
				}
				
				if ($application -eq "content api") {
					echo "Please stop 192.168.250.183"
				}
				
                pause
			}

            echo "Stopping service on $hostedname"
			invoke-command -ComputerName $SecondaryHostName -ScriptBlock {stop-service w3svc} -credential $cred
            echo "Copying to $hostedname"
            if ($application -ne 'content api') {
	    	    copy "C:\$directory\$application\*" -destination "\\$SecondaryHostName\c$\$directory\$application\" -recurse -force -exclude client.json, server.json, appsettings.Production, config.json
            }

            if ($application -eq 'content api') {
	    	    copy "C:\$directory\$copydirectory\*" -destination "\\$SecondaryHostName\c$\$directory\$copydirectory\" -recurse -force -exclude client.json, server.json, appsettings.Production, config.json
            }

            if ($application -eq 'website') {
                Copy-Item -path "\\$PrimaryServer\c$\backup\$BackupDirectory\Sites\website\config\server.json" -Destination "\\$SecondaryHostName\c$\$directory\$application\config\server.json" -force
                Copy-Item -path "\\$PrimaryServer\c$\backup\$BackupDirectory\Sites\website\config\client.json" -Destination "\\$SecondaryHostName\c$\$directory\$application\config\client.json" -force
            }

            if (($application -eq 'content api') -and ($environment -eq 'prod')) {
                $primaryserver = $primaryserver.substring(0,14)
                Copy-Item -path "\\$PrimaryServer\c$\backup\$BackupDirectory\Services\content\appsettings.json" -Destination "\\$SecondaryHostName\c$\$directory\content\appsettings.json" -force
                Copy-Item -path "\\$PrimaryServer\c$\backup\$BackupDirectory\Services\content\appsettings.Production.json" -Destination "\\$SecondaryHostName\c$\$directory\content\appsettings.Production.json" -force
            }
            
            if (($application -eq 'content api') -and ($environment -eq 'stage')) {
                $primaryserver = $primaryserver.substring(0,14)
                Copy-Item -path "\\$PrimaryServer\c$\backup\$BackupDirectory\Services\content\appsettings.json" -Destination "\\$SecondaryHostName\c$\$directory\content\appsettings.json" -force
            }
            #get-childitem "C:\$directory\$application" -Exclude $exclusions | Copy-Item -destination "\\$SecondaryHostName\c$\$directory\$application\" -recurse -force
            #Cleaning up files created by get-childitem recursive copying
            #rm "\\$SecondaryHostName\c$\$directory\$application\server.json" -ErrorAction SilentlyContinue
            #rm "\\$SecondaryHostName\c$\$directory\$application\client.json" -ErrorAction SilentlyContinue
			echo "Starting service on $hostedname"
            invoke-command -ComputerName $SecondaryHostName -ScriptBlock {start-service w3svc} -credential $cred
			echo "$hostedname has been completed"
        }
	    #This if is for all servers that are above or equal to 10 since the naming will be different.  This happens because $i is an integer and will need to be injected into a string 	
        if ($i -ge 10) {
	    	
            $SecondaryHostName = $SecondaryHosts + $i + ".perimeter.amc.corp"
            $hostedname = $secondaryhosts + $i
            
			if (($stopservername -eq $secondaryhostname) -and ($environment -eq "prod")) {
				write-host 'Press any key once the first half of the F5 nodes have been enabled and the last half have been disabled'
				if ($application -eq "website") {
					echo "Please stop first half of the servers"
				}
				
				if ($application -eq "graph") {
					echo "Please stop first half of the servers"
				}
				
				if ($application -eq "content api") {
					echo "Please stop 192.168.250.181 - 182"
				}
				
                pause
			}
            
            echo "Stopping service on $hostedname"
			#invoke-command -ComputerName $SecondaryHostName -ScriptBlock {stop-service w3svc} -cred $cred
            echo "Copying to $hostedname"
	    	copy "C:\$directory\$application\*" -destination "\\$SecondaryHostName\c$\$directory\$application\" -recurse -force -exclude client.json, server.json, appsettings.Production, config.json
            #get-childitem "C:\$directory\$application" -Exclude $exclusions | Copy-Item -destination "\\$SecondaryHostName\c$\$directory\$application\" -recurse -force
            #Cleaning up files created by get-childitem recursive copying
            if ($application -eq 'website') {
                Copy-Item -path "\\$PrimaryServer\c$\backup\$BackupDirectory\Sites\website\config\server.json" -Destination "\\$SecondaryHostName\c$\$directory\$application\config\server.json" -force
                Copy-Item -path "\\$PrimaryServer\c$\backup\$BackupDirectory\Sites\website\config\client.json" -Destination "\\$SecondaryHostName\c$\$directory\$application\config\client.json" -force
            }
			echo "Starting service on $hostedname"
            invoke-command -ComputerName $SecondaryHostName -ScriptBlock {start-service w3svc} -cred $cred
			echo "$hostedname has been completed"
        }
	
        $i++
	
    } while ($i -lt $servernumber)

    if ($application -ne 'content api') {
        if (($application -ne 'graph') -and ($environment -ne 'prod')) {
            test -application $application -environment $environment -directory $directory -distro $distro
        }
    }
    
    #Cleanup of secure string file containing user credentials
	rm C:\Users\$username\documents\securestring.txt
    
if (($environment -eq 'prod') -or ($StageTestMode -eq 'true')){
    if ($application -eq "website") {
		echo "Please re-enable 192.168.250.175 - 192.168.250.178"
	}
				
	if ($application -eq "graph") {
		echo "Please re-enable 192.168.250.198 - 192.168.250.204"
	}

	if ($application -eq "content api") {
		echo "Please re-enable 192.168.250.183"
	}
}

    pause
	#rm C:\Temp\* -recurse    