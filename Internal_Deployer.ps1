﻿param(
    [string]$ECAusername,
    [string]$ECApassword,
    [string]$application,
    [string]$environment
)

$username = $env:USERNAME
#$ECAusername = "ecaJAR"
$primaryhost = hostname
Add-Type -AssemblyName Microsoft.VisualBasic
Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing
Add-PSSnapin iControlSnapin

#$environment = "Prod"
#$environment > C:\tester\outlook.txt
$ECAsecurepassword = ConvertTo-SecureString -string $ECApassword -asPlaintext -force

#$credential = $args[1]

<#param (
    [string]$ECAusername,
    [string]$ECApassword
)#>
#echo $ECAusername
#echo $ECApassword
$credentials = New-Object System.Management.Automation.PSCredential ($ECAusername, $ECAsecurepassword)

#echo $credential > C:\tester\outlook.txt
#exit
#############   User Change Variables ################

$StageTestMode = "false"

#Prod
if ($StageTestMode -eq "false") {
    $InternalF5IP = "10.96.69.86"
}
#Stage
if ($StageTestMode -eq "true") {
    $InternalF5IP = "10.96.69.182"
}
$GraphPackageDirectory = 'GraphQL (Prod - On Commit)'
$ContentAPIPackageDirectory = 'Content Api (Prod - On Commit)'
$ContentAdminPackageDirectory = 'Content Admin (Prod - On Commit)'
$BuildPackageDirectory = 'svd0tfsbldd01'

#############   Form Building   ######################


$outputform = New-Object System.Windows.Forms.Form
$outputform.text = "Deployment Output"
$outputform.StartPosition = 4
$outputform.Size = New-Object System.Drawing.Size(400,400)

$outputlabel = New-Object System.Windows.Forms.Label
$outputlabel.Location = New-Object System.Drawing.Point(20,20)
$outputlabel.Size = New-Object System.Drawing.Size(150,20)
$outputlabel.Text = 'Output: '
$outputform.Controls.Add($outputlabel)

$outputtextBox = New-Object System.Windows.Forms.TextBox
$outputtextBox.Multiline = $True
$outputtextBox.ScrollBars = "vertical"
$outputtextBox.readonly = $true
$outputtextBox.Location = New-Object System.Drawing.Point(25,50)
$outputtextBox.Size = New-Object System.Drawing.Size(325,250)
$outputform.Controls.Add($outputtextBox)

$outputokbutton = New-Object System.Windows.Forms.Button
$outputokbutton.Location = New-Object System.Drawing.Point(150,325)
$outputokbutton.size = New-Object System.Drawing.Size(75,23)
$outputokbutton.text = "Finished"
#$outputokbutton.Add_Click($button_click)
$outputokbutton.DialogResult = [System.Windows.Forms.DialogResult]::OK
$outputform.Controls.Add($outputokbutton)

$outputform.TopMost = $true

#$outputresult = $outputform.ShowDialog()

if ($outputresult -eq [System.Windows.Forms.DialogResult]::OK)
{
    $outputform.close()
}

#############   Verification Testing  ################

function test() {

    param(
        $application,
        $environment,
        $directory,
        $distro,
        $server_array
    )

    $t = 0

    $verif_ip_list = $server_array

    <#if ($environment -eq "test") {
       $verif_ip_list = @('10.96.72.70', '10.96.77.232', '10.96.77.233', '10.96.77.234')
       $verif_ip_list = @('SVD0WGPHT01', 'SVD0WGPHT02')
    }

    if ($environment -eq "stage") {
        $verif_ip_list = @('something')
    }#>

    Add-OutputBoxLine -Message "`r`n"
    Add-OutputBoxLine -Message "Verification Testing"
    Add-OutputBoxLine -Message "`r`n"

    if ($application -eq 'graph') {
        do {
            $HTTP_Response = $null
            $ip = $verif_ip_list[$t]
            $HTTP_Request = [system.net.webrequest]::create("http://$ip/query")
            
            $ExpectedHTTP = 200
            
            $HTTP_Response = $HTTP_Request.getresponse()

            $HTTP_Status = [int]$HTTP_Response.statuscode

            Add-OutputBoxLine -Message "The HTTP response for $ip is $HTTP_Status"

            echo "The HTTP response for $ip is $HTTP_Status"

            if ($HTTP_Status -ne $ExpectedHTTP) {
                
                Add-OutputBoxLine -Message "THE VERIFICATION TEST HAS FAILED"
                $outputresult = $outputform.ShowDialog()
                #echo "THE VERIFICATION TEST HAS FAILED"
                exit
            }

            $HTTP_Response.close()

            $t++
        } while ($verif_ip_list[$t] -ne $null)
    }

    if ($application -eq 'content api') {
        do {
            $HTTP_Response = $null
            $ip = $verif_ip_list[$t]
            $HTTP_Request = [system.net.webrequest]::create("http://$ip/")
            $ExpectedHTTP = 200
            $HTTP_Response = $HTTP_Request.getresponse()

            $HTTP_Status = [int]$HTTP_Response.statuscode
            
            Add-OutputBoxLine -Message "The HTTP response for $ip is $HTTP_Status"
            echo "The HTTP response for $ip is $HTTP_Status"

            if ($HTTP_Status -ne $ExpectedHTTP) {
                Add-OutputBoxLine -Message "THE VERIFICATION TEST HAS FAILED"
                $outputresult = $outputform.ShowDialog()
                #echo "THE VERIFICATION TEST HAS FAILED"
                exit
            }

            $HTTP_Response.close()

            $t++
        } while ($verif_ip_list[$t] -ne $null)
    }

    Add-OutputBoxLine -Message "`r`n"

    #$outputtextBox.Text = $check

}

######################################################

###########   Functions   ###############

Function Add-OutputBoxLine {
    Param ($Message)
    $OutputtextBox.AppendText("`r`n$Message")
    $OutputtextBox.Refresh()
    $OutputtextBox.ScrollToCaret()
}

Add-OutputBoxLine "This was run on the $application application"
Add-OutputBoxLine "This was run on the $environment environment"

function ServerNumber() { 

    param(
        $application,
        $environment
    )

    $server_list = @()

    $primarylength = $primaryhost.length

    $environmenttype = $primaryhost.substring($primarylength-3,1)

    $applicationtype = $primaryhost.substring($primarylength-6,3)

    #echo $applicationtype > C:\tester\outlook.txt

    if ($application -eq "graph") {
        $applicationtype = 'GPH'
    }

    if ($application -eq "content api") {
        $applicationtype = 'CON'
    }

    if ($application -eq "content admin") {
        $applicationtype = 'CA'
    }

    if ($environment -eq "test") {
        $environmenttype = 'T'
    }

    if ($environment -eq "stage") {
        $environmenttype = 'S'
    }

    if ($environment -eq "prod") {
        $environmenttype = 'P'
    }

    $f = 1

    do { 

        if ($f -lt 10) {
            $server = 'SVD0W' + $applicationtype  + $environmenttype + '0' + $f.ToString()
            $server_list += $server
        }

        if ($f -ge 10) {
            $server = 'SVD0W' + $applicationtype + $environmenttype + $f.ToString()
            $server_list += $server
        }
        $pingerror = Test-Connection -computer $server -Quiet
        
        $f++        
    } while ($pingerror -eq "True")

    $servernumber = $f - 1


    $return = @()

    $return += $servernumber
    $return += $server_list

    echo $return

    return $return

}

#Cleanup of drive connected to remote servers that may exist from previous runs
Remove-PSDrive -name "P" -ErrorAction SilentlyContinue

if (test-path "C:\users\$username\documents\temp\") {
    rm C:\users\$username\documents\temp\ -recurse
}

#Function to send enable or disable commands to F5 servers.  Also contains the list of servers that will be disabled per application
function F5-Controller()
{
    param(
        $toggle,
        $application,
        $environment,
        $credentials,
        $iplist,
        $primary,
        $internalF5IP
    )

    Add-PSSnapin iControlSnapin;

    if ($toggle -eq "Enable") {
        $toggle = "Enabled"
    }

    if ($toggle -eq "Disable") {
        $toggle = "Offline"
    }

    $t = 0

    if (($application -eq "graph") -and ($environment -eq "prod")) {
            $memberbranch = "graph.amctheatres.com.app"
            $pool = "graph.amctheatres.com_pool"
            if ($primary -eq "true") {
                $iplist = @("10.96.86.191", "10.96.86.192", "10.96.86.193", "10.96.86.194", "10.96.86.195")
            }

            if ($primary -eq "false") {
                $iplist = @("10.96.86.196", "10.96.86.197", "10.96.86.198", "10.96.176.199", "10.96.176.200")
            }
    }

    if (($application -eq "graph") -and ($environment -eq "stage")) {
            $memberbranch = "graph.amctheatres.com.app"
            $pool = "graph.amctheatres.com_pool"
            if ($primary -eq "true") {
                $iplist = @("192.168.231.101", "192.168.231.111", "192.168.231.121", "192.168.231.151")
            }

            if ($primary -eq "false") {
                $iplist = @("192.168.231.31", "192.168.231.41", "192.168.231.51", "192.168.231.61")
            }
    }
        
    if ($application -eq "content api") {
            $memberbranch = "content.amctheatres.com"
            $pool = "content.amctheatres.com_pool"
            if ($primary -eq "true") {
                $iplist = @("10.96.86.181", "10.96.86.182")
            }
            if ($primary -eq "false") {
                $iplist = @("10.96.86.183")
            }
    }

    do {

        $ip = $iplist[$t]

        $Member = $ip+":"+"80"
        #Write-Host "Member is $ip"

        # Gets the hostname of the current machine being worked on in F5
        $curhost = hostname
        #IP address of the internal production F5 server
        
        Initialize-F5.iControl -HostName $InternalF5IP -Credentials $credentials
        $ic = get-f5.icontrol
        $ic.systemsession.set_active_folder("/Common")
        $ic.systemsession.set_recursive_query_state("STATE_ENABLED")

        #First if statement is for setting node to offline
        If ($toggle -eq "Enabled")
        {
            Write-Host "Setting $ip to $toggle in pool";
            $ic.locallbnodeaddress.set_monitor_state($ip, "STATE_ENABLED")
            $ic.locallbnodeaddress.set_session_enabled_state($ip, "STATE_ENABLED")
        }    
        If ($toggle -eq "Offline") 
        {
            Write-Host "Setting $ip to $toggle in pool";
            $ic.locallbnodeaddress.set_monitor_state($ip, "STATE_DISABLED")
            $ic.locallbnodeaddress.set_session_enabled_state($ip, "STATE_DISABLED")
            #Set-F5.LTMPoolMemberState -Pool "/Common/$memberbranch/$Pool" -Member $Member -state Offline
        }
        $t++
    }
    while ($iplist[$t] -ne $null)

    start-sleep 8
}

#Main block of code where copying of files occurs on primary server.  This script block is what is run on the primary server on your local machine
$ScriptBlockContent = 
{
    $username = $args[0]
    $LatestFile = $args[1]
    $distro = $args[2]
    $application = $args[3]
    $servernumber = $args[4]
    $PrimaryHost = hostname
    $SecondaryHosts = ''
    $i = $args[9]
    $credentials = $args[5]
    $environment = $args[6]
    $primary = $args[8]
    $prodf5 = $args[7]
    
    $directory = ''

    $returnhostname = @()

    $date = Get-Date
    $month = $date.month
    if ($month -lt 10) {
        $month = "0" + $month
    }

    $day = $date.day
    if ($day -lt 10) {
        $day = "0" + $day
    }

    $year = $date.Year

    $BackupDirectory = $month + "-" + $day + "-" + $year

    #Uses fed in application variables to determine directory structure
    if ($application -eq 'website') {
        $directory = 'Sites'
    }

    if ($application -eq 'graph') {
        $directory = 'Services'
    }

    if ($application -eq 'content api') {
        $directory = 'Services'
    }

    if ($application -eq 'content admin') {
        $directory = 'Sites'
    }

    if (($application -ne 'website') -and ($application -ne 'graph') -and ($application -ne 'content admin') -and ($application -ne 'content api')) {
        Add-OutputBoxLine -Message "Directory copy structure not found"
        $outputresult = $outputform.ShowDialog()
        #echo "Directory copy structure not found"
        exit
    }

    #Creating secondary hostname template using primary hostname
    $PrimaryHostLength = $PrimaryHost.length - 2
    $hostedname = $PrimaryHost
    $PrimaryHost = $PrimaryHost.substring(0,$PrimaryHostLength)

    $SecondaryHosts = $PrimaryHost
    
    $SecondaryHostName = $PrimaryHost + "02"
    
    #This if statement is so that the script block knows to only work on the primary server, this happens because when the F5 servers are swapped, we don't want to work on the primary 
    #server again on the second half
    if ($primary -eq "true") {
        C:\BackupWebsites.bat | out-null

        stop-service W3SVC
        if ($application -eq 'graph') {
            #copy "C:\Temp\temp\Content\C_C\J\workspace\$distro\dist\*" -destination C:\Services\$application\ -recurse -force
            copy "C:\Temp\temp\Content\C_C\J\workspace\$distro\dist\*" -destination C:\$directory\graph -recurse -force -exclude config.json, appsettings.Production
        }

        if ($application -eq 'content admin') {
            #copy "C:\Temp\temp\Content\C_C\J\workspace\$distro\dist\*" -destination C:\Services\$application\ -recurse -force
            copy "C:\Temp\temp\Content\C_C\J\workspace\$distro\src\Amc.Admin.Web\bin\Release\netcoreapp1.1\publish\*" -destination C:\$directory\admin -recurse -force -exclude config.json, appsettings.Production
        }

        if ($application -eq 'content api') {
        
            if ($application -eq 'content api') {
                $extra = 'AmcContentApi-v1'
                $application = 'content'
            }

            copy "C:\Temp\temp\Content\C_C\J\workspace\$distro\src\$extra\bin\Release\netcoreapp1.1\publish\*" -destination C:\$directory\$application\ -recurse -force -exclude config.json, appsettings.Production
            if ($environment -eq "prod") {
                copy "C:\Backup\$BackupDirectory\Services\content\appsettings.json" -Destination C:\$directory\$application\
            }
            if ($environment -eq "stage") {
                copy "C:\Backup\$BackupDirectory\Services\content\appsettings.Staging.json" -Destination C:\$directory\$application\
            }
        }

        #Cleanup of files and starting up services
        rm C:\Temp\temp\ -recurse -force
        start-service W3SVC
        
        #$outputresult = $outputform.ShowDialog()
        echo "$hostedname has been completed`r"
        $returnhostname = $hostedname
    }

    #Servernumber is the number of servers for the application and will skip this section if this is only one server
    if ($servernumber -gt 2) {
        
        do {
           #This if is for all servers that are below 10 since the naming will be different.  This happens because $i is an integer and will need to be injected into a string 	
           if ($i -lt 10) {
            
	        	$SecondaryHostName = $SecondaryHosts + "0" + $i
                #Here we connect to all secondary servers from the primary server using invoke commands and new psdrives to connect remotely.  Once connected, we copy the files over directly
                #from the primary server to ensure all servers have the same files
                Invoke-Command -ComputerName $SecondaryHostName -ScriptBlock {stop-service W3SVC} -Credential $credentials
	        	New-PSDrive -name "P" -PSProvider FileSystem -root "\\$secondaryhostname\c$\$directory" -Credential $credentials | out-null
                copy -path C:\$directory\$application\* -destination P:\$application -Recurse -force -exclude config.json, appsettings.Production
                Remove-PSDrive -name "P"
                Invoke-Command -ComputerName $SecondaryHostName -ScriptBlock {start-service W3SVC} -Credential $credentials
                #Add-OutputBoxLine -Message "$SecondaryHostName has been completed"
                echo "$SecondaryHostName has been completed`r"
                $returnhostname = $SecondaryHostName
                #Breaks out of the loop if we have reached the midway server and need to enable the first half of the F5 nodes and disable the second half

                if (($i -eq $prodf5) -and (($environment -eq 'prod') -or ($environment -eq 'stage'))) {
                    exit
                }
                
            }

	        #This if is for all servers that are above or equal to 10 since the naming will be different.  This happens because $i is an integer and will need to be injected into a string 	
            if ($i -ge 10) {
                
	        	$SecondaryHostName = $SecondaryHosts + $i

	        	Invoke-Command -ComputerName $SecondaryHostName -ScriptBlock {stop-service W3SVC} -Credential $credentials
	        	New-PSDrive -name "P" -PSProvider FileSystem -root "\\$secondaryhostname\c$\$directory" -Credential $credentials | out-null
                copy -path C:\$directory\$application\* -destination P:\$application -Recurse -force -exclude config.json, appsettings.Production
                Remove-PSDrive -name "P"
                Invoke-Command -ComputerName $SecondaryHostName -ScriptBlock {start-service W3SVC} -Credential $credentials
                #Add-OutputBoxLine -Message "$SecondaryHostName has been completed"
                echo "$SecondaryHostName has been completed`r"
                $returnhostname = $SecondaryHostName
                #Breaks out of the loop if we have reached the midway server and need to enable the first half of the F5 nodes and disable the second half
                if (($i -eq $prodf5) -and (($environment -eq 'prod') -or ($environment -eq 'stage'))) {
                    exit
                }
            }
	
            $i++
	
        } while ($i -lt $servernumber)
    }

    return $returnhostname
    
}
    #Prompts to populate variables for application and environment.  These will then be used to determine the directory structure, number of servers, primary server and list of F5 nodes to
    #be used as the first half and last half

    #$IntorExt = read-host -prompt 'Internal or External? (EXTERNAL DOES NOT CURRENTLY WORK)'
    $IntorExt = 'int'
    if ($IntorExt -eq 'int') {
        $IntorExt = 'internal'
    }

    if ($IntorExt -eq 'ext') {
        $IntorExt = 'external'
    }

    if (($IntorExt -ne 'internal') -and ($IntorExt -ne 'external')) {
        echo "Please choose either internal or external"
        exit
    }
    
    #$Application = [Microsoft.VisualBasic.Interaction]::InputBox('Enter the application', 'Application Input')
    
    #$Application = read-host -prompt 'What application? (Graph, Content Admin, Content API)'
    
    #if (($Application -ne 'Graph') -and ($Application -ne 'website') -and ($Application -ne 'payment') -and ($Application -ne 'content admin') -and ($Application -ne 'content api')) {
        #echo "Please choose a valid application"
    #    [System.Windows.Forms.MessageBox]::show('Please choose a valid application', 'Error') | out-null
    #    exit
    #}
    
    #$environment = read-host -prompt 'Prod, Stage, Test or Dev?'
    #$environment = [Microsoft.VisualBasic.Interaction]::InputBox('Enter the environment', 'Environment Input')

    #if (($environment -eq "prod") -and ($StageTestMode -eq "true")) {
    #    [System.Windows.Forms.MessageBox]::show('Please disable Stage Test Mode', 'Error')
    #    exit
    #}

    #if (($environment -ne 'prod') -and ($environment -ne 'stage') -and ($environment -ne 'test') -and ($environment -ne 'dev')) {
        #echo "Please choose either prod, stage, dev or test"
    #    [System.Windows.Forms.MessageBox]::show('Please choose a valid environment', 'Error')
    #    exit
    #}

    if (($IntorExt -eq 'internal') -and ($application -eq 'website')) {
        echo 'There is no internal website'
        exit
    }
    
        #Server number is always one more than number of servers available.  $ProdF5 variable determines the midway point on production servers
        if (($environment -eq 'stage') -and ($application -eq 'graph')) {
            $server = 'SVD0WGPHS01'
            $firstiplist = @("192.168.231.101", "192.168.231.111", "192.168.231.121", "192.168.231.151")
            $secondiplist = @("192.168.231.31", "192.168.231.41", "192.168.231.51", "192.168.231.61")
            #CHANGE ME BACK
            $servernumber = 7
            $prodf5 = $servernumber / 2
            $prodf5 = [math]::floor($prodf5)
            #$servernumber = 5
        }

        if (($environment -eq 'test') -and ($application -eq 'graph') -and ($IntorExt -eq 'internal')) {
            $server = 'SVD0WGPHT01'
            $servernumber = 5
        }

        if (($environment -eq 'dev') -and ($application -eq 'graph') -and ($IntorExt -eq 'internal')) {
            $server = 'SVD0WGPHV01'
            $servernumber = 2
        }

        if (($environment -eq 'prod') -and ($application -eq 'graph') -and ($IntorExt -eq 'internal')) {
            $server = 'SVD0WGPHP01'
            $servernumber = 17
            $firstiplist = @("10.96.86.191", "10.96.86.192", "10.96.86.193", "10.96.86.194")
            $secondiplist = @("10.96.86.195", "10.96.86.196", "10.96.86.197", "10.96.86.198")
            $prodf5 = $servernumber / 2
            $prodf5 = [math]::floor($prodf5)
        }

        if (($environment -eq 'stage') -and ($application -eq 'content admin')) {
            $server = 'SVD0WCAS01'
            $servernumber = 2
        }

        if (($environment -eq 'test') -and ($application -eq 'content admin')) {
            $server = 'SVD0WCAT01'
            $servernumber = 5
        }

        if (($environment -eq 'dev') -and ($application -eq 'content admin')) {
            $server = 'SVD0WCAV01'
            $servernumber = 2
        }

        if (($environment -eq 'prod') -and ($application -eq 'content admin')) {
            $server = '10.96.86.90'
            $servernumber = 2
        }

        if (($environment -eq 'stage') -and ($application -eq 'content api')) {
            $server = 'SVD0WCONS01'
            $servernumber = 3
        }
        
        if (($environment -eq 'prod') -and ($application -eq 'content api')) {
            $server = 'SVD0WCONP01'
            $servernumber = 5
            $firstiplist = @("10.96.86.181", "10.96.86.182")
            $secondiplist = @("10.96.86.183")
            $prodf5 = $servernumber / 2
            $prodf5 = [math]::floor($prodf5)
        }

        if (($environment -eq 'test') -and ($application -eq 'content api')) {
            $server = 'SVD0WCONT01'
            $servernumber = 5
        }

        if (($environment -eq 'dev') -and ($application -eq 'content api')) {
            $server = 'SVD0WCONV01'
            $servernumber = 2
        }

        $return = ServerNumber -application $application -environment $environment

#echo "here"
#echo $servernumber
        $server_array = @()
        $servernumber = $return[0]
        $r = 1
        do {
        #echo $return[$r]
            $server_array += $return[$r]
            $r++
        } while ($r -lt $servernumber)

        $server = $return[1]
        if (($application -ne 'graph') -and ($environment -ne 'prod')) {
            test -application $Application -environment $environment -directory $directory -distro $distro -server_array $server_array
        }
        #$outputform.ShowDialog()
        #exit
        #Credentials object created to be able to invoke commands on remote servers
        #$credentials = New-Object System.Management.Automation.PSCredential(Get-Credential -Credential $ECAusername)
        
        <#if ($credentials.Password.Length -eq 0) {
            Add-OutputBoxLine -Message "No password provided"
            $outputresult = $outputform.ShowDialog()
            #echo "No password provided"
            exit
        }#>

        #$outputresult = $outputform.ShowDialog()
        #exit
        #exit
        #This is a check to see if it is possible to create a remote drive on a server before continuing on with the script
        $error.clear()

        New-PSDrive -name "P" -PSProvider FileSystem -root \\$server\c$\Temp -Credential $credentials | out-null

        if ($error) {
            Add-OutputBoxLine -Message "Invalid Credentials"
            Add-OutputBoxLine -Message "$error"
            $outputresult = $outputform.ShowDialog()
            #echo "Invalid Credentials"
            exit
        }
        
        Remove-PSDrive -name "P" -ErrorAction SilentlyContinue

            #Here we determine the latest file from the build packages for the application selected and will then copy that package to the local temp directory
            new-item -path "C:\users\$username\documents\" -name temp -itemtype "directory" | out-null

            if (($application -eq 'graph') -or ($application -eq 'website')) {
                $latest = Get-ChildItem -Path \\$BuildPackageDirectory\C$\BuildPackages\$application | Sort-Object LastAccessTime -Descending | Select-Object -First 1
                $LatestFile = $latest.name
                copy-item -path \\$BuildPackageDirectory\C$\BuildPackages\$application\$LatestFile -destination C:\users\$username\documents\temp\ 
            }

            if ($application -eq 'content api') {
                $appdetour = 'content-api'
                $latest = Get-ChildItem -Path \\$BuildPackageDirectory\C$\BuildPackages\$appdetour | Sort-Object LastAccessTime -Descending | Select-Object -First 1
                $LatestFile = $latest.name
                copy-item -path \\$BuildPackageDirectory\C$\BuildPackages\$appdetour\$LatestFile -destination C:\users\$username\documents\temp\
            }

            if ($application -eq 'content admin') {
                $appdetour = 'content-admin'
                $latest = Get-ChildItem -Path \\$BuildPackageDirectory\C$\BuildPackages\$appdetour | Sort-Object LastAccessTime -Descending | Select-Object -First 1
                $LatestFile = $latest.name
                copy-item -path \\$BuildPackageDirectory\C$\BuildPackages\$appdetour\$LatestFile -destination C:\users\$username\documents\temp\
            }

            #Unzipping package onto local temp directory
            Expand-Archive -literalpath C:\users\$username\documents\temp\$LatestFile -DestinationPath C:\users\$username\documents\temp\

            if (test-path "C:\$server\C$\temp\temp") {
                rmdir C:\$server\C$\temp\temp -recurse
            }

            #Creating a new drive on the local system that connects directly to the primary server and then copies over the unzipped contents of the application package
            New-PSDrive -name "P" -PSProvider FileSystem -root \\$server\c$ -Credential $credentials | out-null

            if (test-path "P:\temp\temp") {
                rmdir p:\temp\temp -recurse
            }

            copy -path "C:\Users\$username\Documents\temp\" -destination "P:\Temp" -recurse -Exclude config.json, server.json, client.json, appsettings.Production

            #Cleaning up session to primary server
            Remove-PSSession -name "P"

            #Creates a session for the primary server to use invoke commands later on
            $s = new-pssession -computername $server -Credential $credentials
            
            #Determines the package directory structure of each application
            if ($application -eq 'graph') {
                $distro = $GraphPackageDirectory
            }

            if ($application -eq 'website') {
                $distro = 'Website (Prod - On Commit)'
            }

            if ($application -eq 'payment') {
                $distro = 'Payment (Prod - On Commit)'
            }

            if ($application -eq 'content api') {
                $distro = $ContentAPIPackageDirectory
            }

            if ($application -eq 'content admin') {
                $distro = $ContentAdminPackageDirectory
            }

            
            
            #Invoke commands that will go back to the top of the script to run copies depending on which application and environment was chosen.  Also determines if the copies will begin at the primary server
            #or on a different server for the F5 nodes
            if (($environment -ne "prod") -and ($environment -ne "stage")) {
                $start = 2
                $primary = "true"
                Invoke-Command -session $s -ScriptBlock $ScriptBlockContent -argumentlist $username, $LatestFile, $distro, $Application, $servernumber, $credentials, $environment, $prodf5, $primary, $start

            }

            if (($environment -eq "stage") -and ($StageTestMode -eq "false")) {
                $start = 2
                $primary = "true"
                $prodf5 = 1000
                $returnedhostname = Invoke-Command -session $s -ScriptBlock $ScriptBlockContent -argumentlist $username, $LatestFile, $distro, $Application, $servernumber, $credentials, $environment, $prodf5, $primary, $start
                Add-OutputBoxLine -Message "$returnedhostname`r"
                
                #Add-OutputBoxLine -Message "Here's a line`r`n"
                #Add-OutputBoxLine -Message "$props.PrimaryHost"
                #Add-OutputBoxLine -Message "$props.SecondaryHostname"
            }

            if (($environment -eq "stage") -and ($application -eq "graph") -and ($StageTestMode -eq "true")) {
                $start = 2
                $primary = "true"

                echo "Disabling first block of F5 Load Balancers"
                
                F5-Controller -toggle "Disable" -application $application -environment $environment -credentials $credentials -iplist $firstiplist -primary $primary -internalF5IP $InternalF5IP
                
                start-sleep 10

                Invoke-Command -session $s -ScriptBlock $ScriptBlockContent -argumentlist $username, $LatestFile, $distro, $Application, $servernumber, $credentials, $environment, $prodf5, $primary, $start

                echo "Enabling first block of F5 Load Balancers"

                F5-Controller -toggle "Enable" -application $application -environment $environment -credentials $credentials -iplist $firstiplist -primary $primary -internalF5IP $InternalF5IP

                start-sleep 10

                $start = $prodf5 + 1
                $prodf5 = 1000
                $primary = "false"
                #pause

                $s = new-pssession -computername $server -Credential $credentials

                echo "Disabling second block of F5 Load Balancers"

                F5-Controller -toggle "Disable" -application $application -environment $environment -credentials $credentials -iplist $secondiplist -primary $primary -internalF5IP $InternalF5IP

                start-sleep 10
                
                Invoke-Command -session $s -ScriptBlock $ScriptBlockContent -argumentlist $username, $LatestFile, $distro, $Application, $servernumber, $credentials, $environment, $prodf5, $primary, $start

                echo "Enabling second block of F5 Load Balancers"

                F5-Controller -toggle "Enable" -application $application -environment $environment -credentials $credentials -iplist $secondiplist -primary $primary -internalF5IP $InternalF5IP

                start-sleep 5
            }

            if (($environment -eq "prod") -and (($application -ne "graph") -and ($application -ne "content api") -and ($application -ne "content admin"))) {
                $start = 2
                $primary = "true"
                Invoke-Command -session $s -ScriptBlock $ScriptBlockContent -argumentlist $username, $LatestFile, $distro, $Application, $servernumber, $credentials, $environment, $prodf5, $primary, $start
            }

            if (($environment -eq "prod") -and (($application -eq "graph") -or ($application -eq "content api") -or ($application -eq "content admin"))) {
                $start = 2
                $primary = "true"

                echo "Disabling first block of F5 Load Balancers"
                
                F5-Controller -toggle "Disable" -application $application -environment $environment -credentials $credentials -iplist $firstiplist -primary $primary -internalF5IP $InternalF5IP
                
                start-sleep 5

                Invoke-Command -session $s -ScriptBlock $ScriptBlockContent -argumentlist $username, $LatestFile, $distro, $Application, $servernumber, $credentials, $environment, $prodf5, $primary, $start

                echo "Enabling first block of F5 Load Balancers"

                F5-Controller -toggle "Enable" -application $application -environment $environment -credentials $credentials -iplist $firstiplist -primary $primary -internalF5IP $InternalF5IP

                start-sleep 5

                $start = $prodf5 + 1
                $prodf5 = 1000
                $primary = "false"
                #pause

                $s = new-pssession -computername $server -Credential $credentials

                echo "Disabling second block of F5 Load Balancers"

                F5-Controller -toggle "Disable" -application $application -environment $environment -credentials $credentials -iplist $secondiplist -primary $primary -internalF5IP $InternalF5IP

                start-sleep 5

                Invoke-Command -session $s -ScriptBlock $ScriptBlockContent -argumentlist $username, $LatestFile, $distro, $Application, $servernumber, $credentials, $environment, $prodf5, $primary, $start

                echo "Enabling second block of F5 Load Balancers"

                F5-Controller -toggle "Enable" -application $application -environment $environment -credentials $credentials -iplist $secondiplist -primary $primary -internalF5IP $InternalF5IP

                start-sleep 5

            }

            #Cleaning up session for next run
            remove-pssession -computername $server

    #Cleaning up local temp directory
    if (test-path "C:\users\$username\documents\temp\") {
        rm C:\users\$username\documents\temp\ -recurse
    }
    if (($application -ne 'graph') -and ($environment -ne 'prod')) {
        test -application $Application -environment $environment -directory $directory -distro $distro -server_array $server_array
    }

    #Cleaning up remote drives
    Remove-PSDrive -name "P"

    
$outputresult = $outputform.ShowDialog()


    #pause