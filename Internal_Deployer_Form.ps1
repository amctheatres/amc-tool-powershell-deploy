﻿Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing

$username = $env:USERNAME

$environment = $null
$application = $null

$button_click = {

    $application = $applicationlistbox.SelectedItem
    
    if ($application -eq $null) {
        [System.Windows.Forms.MessageBox]::show('Application Missing', 'Error') | out-null
        $form.close()
        exit
    }

    $environment = $environmentlistBox.SelectedItem

    if ($environment -eq $null) {
        [System.Windows.Forms.MessageBox]::show('Environment Missing', 'Error') | out-null
        $form.close()
        exit
    }

    if ($cyberarkusertextbox.Text.Length -eq 0) {
        [System.Windows.Forms.MessageBox]::show('Username Missing', 'Error') | out-null
        $form.close()
        exit
    }

    $ECAusername = $cyberarkusertextBox.Text

    if ($cyberarkpasstextbox.Text.Length -eq 0) {
        [System.Windows.Forms.MessageBox]::show('Password Missing', 'Error') | out-null
        $form.close()
        exit
    }

    $ECApassword = $cyberarkpasstextBox.Text

    C:\Users\$username\Documents\amc-tool-powershell-deploy\Internal_Deployer.ps1 -ECAusername $ECAusername -ecapassword $ECApassword -application $application -environment $environment

}

$form = New-Object System.Windows.Forms.Form
$form.text = "Powershell Deployment Inputs"
$form.StartPosition = 4
$form.Size = New-Object System.Drawing.Size(600,600)

$okbutton = New-Object System.Windows.Forms.Button
$okbutton.Location = New-Object System.Drawing.Point(200,500)
$okbutton.size = New-Object System.Drawing.Size(75,23)
$okbutton.text = "Run"
$okbutton.Add_Click($button_click)
$okbutton.DialogResult = [System.Windows.Forms.DialogResult]::OK
$form.AcceptButton = $okbutton
$form.Controls.Add($okbutton)

$CancelButton = New-Object System.Windows.Forms.Button
$CancelButton.Location = New-Object System.Drawing.Point(300,500)
$CancelButton.Size = New-Object System.Drawing.Size(75,23)
$CancelButton.Text = 'Cancel'
$CancelButton.DialogResult = [System.Windows.Forms.DialogResult]::Cancel
$form.CancelButton = $CancelButton
$form.Controls.Add($CancelButton)

$Mainlabel = New-Object System.Windows.Forms.Label
$Mainlabel.Location = New-Object System.Drawing.Point(100,20)
$Mainlabel.Size = New-Object System.Drawing.Size(500,20)
$Mainlabel.Text = 'Please provide the application, environment and CyberArk password'
$form.Controls.Add($Mainlabel)

$applicationlabel = New-Object System.Windows.Forms.Label
$applicationlabel.Location = New-Object System.Drawing.Point(50,70)
$applicationlabel.Size = New-Object System.Drawing.Size(100,20)
$applicationlabel.Text = 'Select application: '
$form.Controls.Add($applicationlabel)

$applicationlistBox = New-Object System.Windows.Forms.Listbox
$applicationlistBox.Location = New-Object System.Drawing.Point(50,100)
$applicationlistBox.Size = New-Object System.Drawing.Size(120,20)

$applicationlistBox.SelectionMode = 'One'

[void] $applicationlistBox.Items.Add('Graph')
[void] $applicationlistBox.Items.Add('Content API')
[void] $applicationlistBox.Items.Add('Content Admin')

$applicationlistBox.Height = 95
$form.Controls.Add($applicationlistBox)

$environmentlabel = New-Object System.Windows.Forms.Label
$environmentlabel.Location = New-Object System.Drawing.Point(200,70)
$environmentlabel.Size = New-Object System.Drawing.Size(150,20)
$environmentlabel.Text = 'Select environment: '
$form.Controls.Add($environmentlabel)

$environmentlistbox = New-Object System.Windows.Forms.Listbox
$environmentlistbox.Location = New-Object System.Drawing.Point(200,100)
$environmentlistbox.Size = New-Object System.Drawing.Size(120,20)

$environmentlistbox.SelectionMode = 'One'

[void] $environmentlistbox.Items.Add('Prod')
[void] $environmentlistbox.Items.Add('Stage')
[void] $environmentlistbox.Items.Add('Test')
[void] $environmentlistbox.Items.Add('Dev')

$environmentlistbox.Height = 95
$form.Controls.Add($environmentlistbox)

$cyberarkuserlabel = New-Object System.Windows.Forms.Label
$cyberarkuserlabel.Location = New-Object System.Drawing.Point(350,70)
$cyberarkuserlabel.Size = New-Object System.Drawing.Size(150,20)
$cyberarkuserlabel.Text = 'CyberArk Username: '
$form.Controls.Add($cyberarkuserlabel)

$cyberarkusertextBox = New-Object System.Windows.Forms.TextBox
$cyberarkusertextBox.Location = New-Object System.Drawing.Point(350,100)
$cyberarkusertextBox.Size = New-Object System.Drawing.Size(150,20)
$form.Controls.Add($cyberarkusertextBox)

$cyberarkpasslabel = New-Object System.Windows.Forms.Label
$cyberarkpasslabel.Location = New-Object System.Drawing.Point(350,140)
$cyberarkpasslabel.Size = New-Object System.Drawing.Size(150,20)
$cyberarkpasslabel.Text = 'CyberArk Password: '
$form.Controls.Add($cyberarkpasslabel)

$cyberarkpasstextBox = New-Object System.Windows.Forms.MaskedTextBox
$cyberarkpasstextBox.PasswordChar = '*'
$cyberarkpasstextBox.Location = New-Object System.Drawing.Point(350,170)
$cyberarkpasstextBox.Size = New-Object System.Drawing.Size(150,20)
$form.Controls.Add($cyberarkpasstextBox)

$result = $form.ShowDialog()

if ($result -eq [System.Windows.Forms.DialogResult]::OK)
{

#$password = ConvertTo-SecureString "" -AsPlainText -Force

    #$credentials = New-Object System.Management.Automation.PSCredential ('ecaJAR', $ECApassword)

    #$credentials = [pscredential]::new($ECAusername, $ECApassword)

    
    <#$application = $applicationlistbox.SelectedItem

    if ($application -eq $null) {
        [System.Windows.Forms.MessageBox]::show('Application Missing', 'Error') | out-null
        exit
    }

    $environment = $environmentlistBox.SelectedItem

    if ($environment -eq $null) {
        [System.Windows.Forms.MessageBox]::show('Environment Missing', 'Error') | out-null
        exit
    }#>
    
}
    
   #$outputtextBox.Text = "here"

if ($result -eq [System.Windows.Forms.DialogResult]::Cancel) {
    exit
}

